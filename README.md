# Pipelines Laravel Envoy Example

Pipelines Laravel Envoy Example.

## Setup

Enable pipelines in your repository by visiting the Bitbucket GUI `Settings -> Pipelines -> Settings -> Enable`.

### SSH Keys

Since we will be using SSH to connect to the remote server we can take advantage of _passwordless_ logins by using public key authentication! You can either use your own existing SSH keys or generate unique ones that are tied to Bitbucket - I suggest the latter.

Browser to `Settings -> Pipelines -> SSH Keys` in the Bitbucket GUI and follow the instructions [here](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html). Basically it involves two simple steps:

* Add SSH Key
* Add Known Hosts (ip or hostname)

### Required Environment Variables

Setup these variables via the Bitbucket GUI `Settings -> Pipelines -> Environment variables` these are used in the bitbucket-pipelines.yml script and passed into Envoy.blade.php.

* `HOST` ip or hostname of the remote server.
* `USER` username of the SSH user.

### Define Envoy Tasks

You are free to define as many tasks as required, these go in the Envoy.blade.php file. Tasks are grouped together as 'Stories' which can then be invoked from the command line via `envoy run %STORY_NAME%` - in this repository example there's a story called 'exampleCommands' with two attached tasks (pipeline_task and server_task) which is invoked from bitbucket-pipelines.yml on line 21.
